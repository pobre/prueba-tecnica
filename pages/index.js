import React, { useEffect } from 'react'
import Head from 'next/head'
import ReactTable from 'react-table-v6'
import {getPosts} from '../components/services/placeholder'
import useState from 'react-usestateref'
import Modal from '../components/utils/modal'

export default function Home() {

  var [data, setData, dataRef] = useState([])
  var [columns, setColumns, columnsRef] = useState([])

  var [close, setClose, closeRef] = useState(false)

  var [editable, setEditable, editableRef] = useState({})

  var [booEdit, setBooEdit, booEditRef] = useState(false)

  useEffect(() => {

    setColumns([
      {Header: 'ACCIONES', accessor: 'acciones', filterable: false},
      {Header: 'ID', accessor: 'id'},
      {Header: 'NAME', accessor: 'name'},
      {Header: 'USERNAME', accessor: 'username'},
      {Header: 'EMAIL', accessor: 'email'}
    ])

    getPosts().then(res=>{

      if(res.status === 200){
        let new_data = res.data
        new_data.forEach((elem,i) =>{
          let obj = {obj: (
            <div className="flex space-x-3 justify-center items-center">
              <svg onClick={()=>{
                setEditable(elem)
                setClose(true)
              }} className="w-6 h-6 text-blue-500 cursor-pointer" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" /></svg>
              <svg onClick={()=>eliminar(elem)} className="w-6 h-6 text-red-500 cursor-pointer" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" /></svg>
            </div>
          )}
          elem.acciones = obj['obj']
        })


        setData(res.data)
      }

    })
  }, [])


  const eliminar = (elem) =>{
    let new_data = []
    dataRef.current.forEach(e=>{
      if(e.id !== elem.id){
        new_data.push(e)
      }
    })

    setData(new_data)
  }

  const pre_editar = (accessor, value) =>{
    let edit = editableRef.current
    edit[accessor] = value
    setEditable({})
    setTimeout(() => {
      setEditable(edit)
    }, 1);
  }

  const editar = () =>{

    let old_data = dataRef.current
    let new_data = []

    old_data.forEach(elm=>{
      if(elm.id === editableRef.current.id){
        elm = editableRef.current
      }
      new_data.push(elm)
    })

    setData(new_data)

    setBooEdit(true)

    setTimeout(() => {
      setBooEdit(false)
    }, 1500);

    setTimeout(() => {
      setClose(false)
    }, 2000);

  }

  return (
    <>
      <Head>
        <title>Prueba técnica</title>
        <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://unpkg.com/react-table-v6@latest/react-table.css" />
      </Head>



      {closeRef.current &&
        <Modal header="Editar trabajador" close={setClose}>
          <div className="w-full">
            <input value={editableRef.current.name} onChange={(e)=>{
              pre_editar('name', e.target.value)
            }} className="px-3 py-2 border-2 border-black focus:outline-none focus:border-blue-500 w-full rounded-lg text-xl" />

            <input value={editableRef.current.username} onChange={(e)=>{
              pre_editar('username', e.target.value)
            }} className="px-3 py-2 border-2 border-black focus:outline-none focus:border-blue-500 w-full rounded-lg text-xl mt-2" />

            <input value={editableRef.current.email} onChange={(e)=>{
              pre_editar('email', e.target.value)
            }} className="px-3 py-2 border-2 border-black focus:outline-none focus:border-blue-500 w-full rounded-lg text-xl mt-2" />


            <button onClick={()=>editar()} className={"duration-300 transition-all text-center font-medium text-xl py-3 rounded-lg w-full mt-4 text-white " + (booEditRef.current ? 'bg-green-500' : 'bg-black')}>
              {booEditRef.current ? 'Editado con éxito' : 'Editar'}
            </button>
          </div>
        </Modal>
      }



      <div className="bg-white justify-center flex h-screen w-full">
        <div className="w-5/6">

          <div className="flex justify-center items-center font-bold text-3xl mt-24">
            Trabajadores
          </div>

          <div className="font-bold text-xl w-full mt-12">
            <ReactTable
              data={dataRef.current}
              columns={columnsRef.current}
              filterable
              pageSizeOptions={[5, 10, 20, 25, 50, 100]}
              defaultPageSize={5}
            />
          </div>

        </div>
      </div>


    </>
  )
}
