import React from 'react'

const modal = (props) => {
    return (
        <div className="fixed w-full h-screen bg-black bg-opacity-40 justify-center flex items-center z-50">
            
            <div className="w-2/6">
                
                {/* HEADER */}
                <div className="bg-gray-100 p-3 flex items-center justify-center rounded-t-xl">
                    <div className="w-2/3">
                        <span className="font-bold camelcase text-xl">{props.header}</span>
                    </div>

                    <div className="w-1/3 justify-end flex">
                        <svg onClick={()=>props.close(false)} className="w-6 h-6 text-red-500 cursor-pointer" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" /></svg>
                    </div>
                </div>

                {/* BODY */}
                <div className="p-3 bg-white">
                    {props.children}
                </div>

            </div>

        </div>
    );
}

export default modal;