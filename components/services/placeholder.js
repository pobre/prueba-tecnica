import axios from 'axios'

let url = 'https://jsonplaceholder.typicode.com'

export const getPosts = () =>{
    return axios
        .get( url + "/users")
        .then(res =>{
            return res
        })
        .catch(err =>{
            return err
        })
}